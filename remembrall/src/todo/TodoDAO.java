package todo;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import member.MemberDAO;
import util.*;

/*
	게시글 Data Access Object
 */
public class TodoDAO {
	Connection conn;
	PreparedStatement pstmt;
	Statement stmt;
	ResultSet rs;
	Logger logger = LoggerFactory.getLogger(MemberDAO.class);
	
	//list
	public ArrayList<Todo> getList(String uid) {
		ArrayList<Todo> datas = new ArrayList<Todo>();
		conn = DBManager.getConnection();
		String sql;

		try {
			sql = "select * from todo where uid=? order by date asc";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,uid);
			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Todo todolist = new Todo();
				
				todolist.setTid(rs.getInt("tid"));
				todolist.setTit(rs.getString("tit"));
				todolist.setDate(rs.getString("date"));
				if(rs.getInt("done")==0) {
					todolist.setDone(false);
				}else if(rs.getInt("done")==1){
					todolist.setDone(true);
				}else {
					todolist.setDone(false);
				}
				datas.add(todolist);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
		}
		finally {
			try {
				//rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.getErrorCode());
			}
		}		
		return datas;
	}
	
	/**
	 * 신규 todo 등록
	 * @param todo
	 * @return
	 */
	public boolean newTodo(Todo todo) {
		conn = DBManager.getConnection();
		String sql = "insert into todo(uid, tid, tit, date) values(?,?,?,?)";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, todo.getUid());
			pstmt.setString(2, String.valueOf(todo.getTid()));
			pstmt.setString(3, todo.getTit());
			pstmt.setString(4, todo.getDate());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;	
	}
	

	/**
	 * todo 완료
	 * @param tid
	 * @return
	 */
	public boolean doneTodo(String tid, String uid, int isDone) {
		conn = DBManager.getConnection();
		String sql = "update todo set done=? where tid=? and uid=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, isDone);
			pstmt.setString(2, tid);
			pstmt.setString(3, uid);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;	
	}
	
	/**
	 * todo 영구 삭제
	 * @param tid
	 * @return
	 */
	public boolean removeTodo(String[] nidArr) {
		System.out.println(nidArr);
		conn = DBManager.getConnection();
		String sql = "delete from todo where tid in(?)";
		int count[] = new int[nidArr.length];
		
		try {
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sql);
			for(int i=0; i<nidArr.length; i++) {
				pstmt.setString(1, nidArr[i]);
				pstmt.addBatch();
			}
			count = pstmt.executeBatch();
			conn.commit();
			//pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		finally {
			try {
				conn.setAutoCommit(true);
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;	
	}
	
	
	
}
