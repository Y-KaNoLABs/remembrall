package todo;

/*
 my note data object
 */


public class Todo {
	// todo 시퀀스 id
	private int tid;
	// 작성자 id
	private String uid;
	// note 제목
	private String tit;
	//작성일
	private String date;
	//완료
	private boolean done;
	
	
	public int getTid() {
		return tid;
	}
	public void setTid(int tid) {
		this.tid=tid;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid=uid;
	}
	public String getTit() {
		return tit;
	}
	public void setTit(String tit) {
		this.tit = tit;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public Boolean getDone() {
		return done;
	}
	public void setDone(Boolean done) {
		this.done= done;
	}
}
