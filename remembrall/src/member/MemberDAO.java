package member;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mynote.Mynote;
import util.*;


/*
 회원 가입, 로그인
 */
public class MemberDAO {
	Connection conn;
	PreparedStatement pstmt;
	ResultSet rs;
	
	Logger logger = LoggerFactory.getLogger(MemberDAO.class);
	
	/**
	 * 신규 회원 등록
	 * @param member
	 * @return
	 */
	public boolean addMember(Member newMember) {
		conn = DBManager.getConnection();
		String sql = "insert into member(name, uid, passwd, email,date) values(?,?,?,?,now())";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, newMember.getName());
			pstmt.setString(2, newMember.getUid());
			pstmt.setString(3, newMember.getPasswd());
			pstmt.setString(4, newMember.getEmail());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Error Code : {}",e.getErrorCode());
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 회원정보 수정 화면
	 * @param uid
	 * @return
	 */
	public ArrayList<Member> getView(String uid) {
		ArrayList<Member> datas = new ArrayList<Member>();
		
		conn = DBManager.getConnection();
		String sql;

		try {
			sql = "select * from member where uid=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,uid);
			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Member memberView= new Member();
				memberView.setName(rs.getString("name"));
				memberView.setPasswd(rs.getString("passwd"));
				memberView.setEmail(rs.getString("email"));
				memberView.setUid(uid);

				datas.add(memberView);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
		}
		finally {
			try {
				//rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.getErrorCode());
			}
		}		
		return datas;
	}
	
	/**
	 * 회원정보 수정 update
	 * @param member, uid
	 * @return
	 */
	public boolean updateMember(Member modMember, String uid) {
		conn = DBManager.getConnection();
		String sql = "update member set name=?, passwd=?, email=? where uid=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, modMember.getName());
			pstmt.setString(2, modMember.getPasswd());
			pstmt.setString(3, modMember.getEmail());
			pstmt.setString(4, uid);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info("Error Code : {}",e.getErrorCode());
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
	
	/**
	 * 회원 로그인
	 * @param uid
	 * @param passwd
	 * @return
	 */
	public boolean login(String uid, String passwd) {
		conn = DBManager.getConnection();
		String sql = "select uid, passwd from member where uid = ?";
		boolean result = false;
		
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, uid);
			rs = pstmt.executeQuery();
			rs.next();
			if(rs.getString("passwd").equals(passwd))
				result=true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return result;
	}
	
	/**
	 * 회원 탈퇴
	 * @param uid
	 * @param passwd
	 * @return
	 */
	public boolean leaveMember(String uid, String passwd) {
		conn = DBManager.getConnection();
		String sql = "delete from member where uid=? and passwd=?"; 
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, uid);
			pstmt.setString(2, passwd);

			pstmt.executeUpdate();
		}catch (SQLException e) {
			e.printStackTrace();
			logger.info("Error Code : {}",e.getErrorCode());
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}