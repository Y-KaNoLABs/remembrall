package member;

import java.util.Date;

/*
 	회원 data object class
 */
public class Member{
	//자동로그인
	private String loginsave;
	//회원 이름
	private String name;
	//회원 id
	private String uid;
	//pw
	private String passwd;
	//email addr
	private String email;
	//가입일
	private Date date;
	
	public String getLoginsave() {
		return loginsave;
	}
	public void setLoginsave(String loginsave) {
		this.loginsave=loginsave;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name=name;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String id) {
		this.uid = id;
	}
	public String getPasswd() {
		return passwd;
	}
	public void setPasswd(String passwd) {
		this.passwd = passwd;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date=date;
	}
}
