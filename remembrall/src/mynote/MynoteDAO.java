package mynote;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import member.MemberDAO;
import util.*;

/*
	게시글 Data Access Object
 */
public class MynoteDAO {
	Connection conn;
	PreparedStatement pstmt;
	Statement stmt;
	ResultSet rs;
	Logger logger = LoggerFactory.getLogger(MemberDAO.class);
	
	//list
	public ArrayList<Mynote> getList(int cnt, String uid) {
		ArrayList<Mynote> datas = new ArrayList<Mynote>();
		conn = DBManager.getConnection();
		String sql;

		try {
			sql = "select * from mynote where uid=? and trashbox=0 order by date desc limit 0,?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,uid);
			pstmt.setInt(2,cnt);
			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Mynote notelist = new Mynote();
				
				notelist.setNid(rs.getInt("nid"));
				notelist.setTit(rs.getString("notetit"));
				notelist.setBody(rs.getString("notebody"));
				notelist.setDate(rs.getDate("date")+" / "+rs.getTime("date"));
				notelist.setMdate(rs.getDate("mdate")+" / "+rs.getTime("mdate"));
				if(rs.getDate("mdate")==null) {
					notelist.setMdate(null);
				}
				
				datas.add(notelist);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
		}
		finally {
			try {
				//rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.getErrorCode());
			}
		}		
		return datas;
	}
	//list count 
	public int getAllCnt(String uid, Boolean isTrash) {
		conn = DBManager.getConnection();
		String sql;
		int allCnt = 0;
		int intIsTrash = 0;
		if(isTrash==true) intIsTrash = 1;

		try {
			sql = "select count(*) from mynote where uid=? and trashbox=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,uid);
			pstmt.setInt(2,intIsTrash);
			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				allCnt = rs.getInt(1);			
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
		}
		finally {
			try {
				//rs.close();
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.getErrorCode());
			}
		}		
		return allCnt;
	}
	
	//trash list
		public ArrayList<Mynote> getTrashList(int cnt, String uid) {
			ArrayList<Mynote> datas = new ArrayList<Mynote>();
			conn = DBManager.getConnection();
			String sql;

			try {
				sql = "select * from mynote where uid=? and trashbox=1 order by date desc limit 0,?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setString(1,uid);
				pstmt.setInt(2,cnt);
				
				ResultSet rs = pstmt.executeQuery();
				while(rs.next()) {
					Mynote notelist = new Mynote();
					
					notelist.setNid(rs.getInt("nid"));
					notelist.setTit(rs.getString("notetit"));
					notelist.setBody(rs.getString("notebody"));
					notelist.setDate(rs.getDate("date")+" / "+rs.getTime("date"));
					
					datas.add(notelist);
				}
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.getErrorCode());
			}
			finally {
				try {
					//rs.close();
					pstmt.close();
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					System.out.println(e.getErrorCode());
				}
			}		
			return datas;
		}
	
	/**
	 * 신규 note 등록
	 * @param note
	 * @return
	 */
	public boolean newNote(Mynote note) {
		conn = DBManager.getConnection();
		String sql = "insert into mynote(uid, nid, notetit, notebody, date) values(?,?,?,?,now())";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, note.getUid());
			pstmt.setString(2, String.valueOf(note.getNid()));
			pstmt.setString(3, note.getTit());
			pstmt.setString(4, note.getBody());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;	
	}
	

	/**
	 * note 수정 -db update
	 * @param note
	 * @return
	 */
	public boolean modifyNote(Mynote note) {
		conn = DBManager.getConnection();
		String sql = "update mynote set notetit=?, notebody=?, mdate=now() where uid=? and nid=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, note.getTit());
			pstmt.setString(2, note.getBody());
			pstmt.setString(3, note.getUid());
			pstmt.setInt(4, note.getNid());
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;	
	}
	
	/**
	 * note 휴지통
	 * @param nid
	 * @return
	 */
	public boolean delNote(String nid, String uid) {
		conn = DBManager.getConnection();
		String sql = "update mynote set trashbox=1 where nid=? and uid=?";
		try {
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, nid);
			pstmt.setString(2, uid);
			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
			return false;
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;	
	}
	
	/**
	 * note 영구 삭제
	 * @param nid
	 * @return
	 */
	public boolean removeNote(String[] nidArr) {
		System.out.println(nidArr);
		conn = DBManager.getConnection();
		String sql = "delete from mynote where nid in(?)";
		int count[] = new int[nidArr.length];
		
		try {
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sql);
			for(int i=0; i<nidArr.length; i++) {
				pstmt.setString(1, nidArr[i]);
				pstmt.addBatch();
			}
			count = pstmt.executeBatch();
			conn.commit();
			//pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		finally {
			try {
				conn.setAutoCommit(true);
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;	
	}
	
	/**
	 * note 휴지통 항목 복구
	 * @param nid
	 * @return
	 */
	public boolean resetNote(String[] nidArr) {
		System.out.println(nidArr);
		conn = DBManager.getConnection();
		String sql = "update mynote set trashbox=0 where nid in(?)";
		int count[] = new int[nidArr.length];
		
		try {
			conn.setAutoCommit(false);
			pstmt = conn.prepareStatement(sql);
			for(int i=0; i<nidArr.length; i++) {
				pstmt.setString(1, nidArr[i]);
				pstmt.addBatch();
			}
			count = pstmt.executeBatch();
			conn.commit();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				conn.rollback();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		finally {
			try {
				conn.setAutoCommit(true);
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return true;	
	}
	
	/**
	 * note view
	 * @param nid, uid
	 * @return
	 */
	public ArrayList<Mynote> getView(String nid, String uid) {
		ArrayList<Mynote> datas = new ArrayList<Mynote>();
		conn = DBManager.getConnection();
		String sql;

		try {
			sql = "select * from mynote where nid=? and uid=?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,nid);
			pstmt.setString(2,uid);
			
			ResultSet rs = pstmt.executeQuery();
			while(rs.next()) {
				Mynote noteview = new Mynote();
				noteview.setTit(rs.getString("notetit"));
				noteview.setBody(rs.getString("notebody"));
				noteview.setDate(rs.getDate("date")+" / "+rs.getTime("date"));
				noteview.setMdate(rs.getDate("mdate")+" / "+rs.getTime("mdate"));
				noteview.setNid(Integer.parseInt(nid));
				noteview.setUid(uid);
				datas.add(noteview);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.getErrorCode());
		}
		finally {
			try {
				pstmt.close();
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
				System.out.println(e.getErrorCode());
			}
		}		
		return datas;
	}
	
}
