package mynote;

/*
 my note data object
 */


public class Mynote {
	// note 시퀀스 id
	private int nid;
	// 작성자 id
	private String uid;
	// note 제목
	private String tit;
	//note 본문
	private String body;
	//작성일
	private String date;
	//수정일
	private String mdate;
	
	
	public int getNid() {
		return nid;
	}
	public void setNid(int nid) {
		this.nid=nid;
	}
	public String getUid() {
		return uid;
	}
	public void setUid(String uid) {
		this.uid=uid;
	}
	public String getTit() {
		return tit;
	}
	public void setTit(String tit) {
		this.tit = tit;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getMdate() {
		return mdate;
	}
	public void setMdate(String mdate) {
		this.mdate = mdate;
	}
}
