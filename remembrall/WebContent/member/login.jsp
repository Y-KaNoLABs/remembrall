<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0, user-scalable=no"/>
<title>로그인 | Remembrall</title>
<link rel="stylesheet" type="text/css" href="../rss/css/font.css" />
<link rel="stylesheet" type="text/css" href="../rss/css/global.css" />
<link rel="stylesheet" type="text/css" href="../rss/css/popup.css" />
<link rel="stylesheet" type="text/css" href="../rss/css/contents.css" />
<script src="../rss/js/lib/jquery-2.2.3.min.js" ></script>
<script src="../rss/js/common.js" ></script>
</head>
<body id="openwin">
	<!-- pop_wrap -->
	<div id="pop_wrap">

		<div class="pop_head">
			<h1>로그인</h1>
		</div>
		
		<!-- pop_cont -->
		<div class="pop_cont">
			
			<form name="loginform" method="post" action="${pageContext.request.contextPath}/member/member_controller.jsp">
				<input type="hidden" name="target" value="popup" />
				<note:login />	
			</form>
		</div>
		<!-- //pop_cont -->
		
		<button type="button" class="cls_btn" onclick="self.close(); return false;" title="창 닫기"><span class="blind">창 닫기</span></button>

	</div>
	<!-- //pop_wrap -->
	<script>
		$('.log_txt .btn_sm_gray').on('click',function(){
			opener.location.href=$(this).attr('href');
			self.close();
		});
		
		$(document).ready(function(){
			console.log(remembrall.util.get_platform());
			if(remembrall.util.get_platform() == 'pc') $('body').removeClass('formobile');
			else  $('body').addClass('formobile');
		});
	</script>
</body>
</html>