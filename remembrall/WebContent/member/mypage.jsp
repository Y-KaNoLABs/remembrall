<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<jsp:include page="../include/header.jsp"/> 
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">Member</a> &gt;
				<span class="current">Mypage</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>Mypage</h1>
				</div>
				<div class="cont_section01">
					<ul class="tab01">
						<li class="on"><a href="./member_controller.jsp?action=modify">회원 정보 수정</a></li>
						<li><a href="leave.jsp">회원 탈퇴</a></li>
					</ul>
				</div>

			</div>
			<!-- //contents -->
		</div>
<jsp:include page="../include/footer.jsp"/> 
<% response.sendRedirect("./member_controller.jsp?action=modify"); %>