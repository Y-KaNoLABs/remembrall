<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<jsp:include page="../include/header.jsp"/> 
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">Member</a> &gt;
				<span class="current">Mypage</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>Mypage</h1>
				</div>
				<div class="cont_section01">
					<div class="cont_section01">
						<ul class="tab01">
							<li class="on"><a href="./member_controller.jsp?action=modify">회원 정보 수정</a></li>
							<li><a href="leave.jsp">회원 탈퇴</a></li>
						</ul>
					</div>
					<form method="post" action="member_controller.jsp?action=update">
					<table class="tb_join" summary="회원정보 수정을 위한 입력 form table">
						<caption>회원정보 수정</caption>
						<colgroup>
							<col style="width:20%" />
							<col />
						</colgroup>
						<tbody>
						<c:forEach varStatus="status" var="n" items="${mdview}">
							<tr><th scope="row">이름</th><td><input type="text" name="name" size="10" value="${n.name}" required /></td></tr>
							<tr><th scope="row">아이디</th><td>${n.uid}</td></tr>
							<tr><th scope="row">이메일</th><td><input type="email" name="email" size="10" value="${n.email}" /></td></tr>
							<tr><th scope="row">비밀번호</th><td><input type="password" name="passwd" size="10" value="${n.passwd}" required /></td></tr>
						</c:forEach>
						</tbody>
					</table>
					<button type="submit" class="submit btn_md_purple fr">수정 완료</button>
					</form>
				</div>
			</div>
			<!-- //contents -->
		</div>
<jsp:include page="../include/footer.jsp"/> 