<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<jsp:include page="../include/header.jsp"/> 
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">Member</a> &gt;
				<span class="current">회원가입</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>회원가입</h1>
				</div>
				<div class="cont_section01">
					<form method="post" action="member_controller.jsp?action=new">
					<table class="tb_join" summary="회원가입을 위한 입력 form table">
						<caption>회원가입</caption>
						<colgroup>
							<col style="width:20%" />
							<col />
						</colgroup>
						<tbody>
							<tr><th scope="row">이름</th><td><input type="text" name="name" size="10" required></td></tr>
							<tr><th scope="row">아이디</th><td><input type="text" name="uid" size="10" required></td></tr>
							<tr><th scope="row">이메일</th><td><input type="email" name="email" size="10"></td></tr>
							<tr><th scope="row">비밀번호</th><td><input type="password" name="passwd" size="10" required></td></tr>
						</tbody>
					</table>
					<button type="submit" class="submit btn_md_purple fr">회원가입</button>
					</form>
				</div>

			</div>
			<!-- //contents -->
		</div>
<jsp:include page="../include/footer.jsp"/> 