<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8" import="mynote.*,member.*,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!-- 메시지 처리 빈즈 -->
<jsp:useBean id="member" class="member.Member" />
<jsp:useBean id="ndao" class="member.MemberDAO" />

<!-- 프로퍼티 set -->
<jsp:setProperty name="member" property="*" />

<%
	//컨트롤러 요청 action 코드값
	String action = request.getParameter("action");
	String target = request.getParameter("target");
	
	// 특정 회원 
		String uid = (String)session.getAttribute("uid");
	
	//신규 회원등록
	if(action.equals("new")){
		if(ndao.addMember(member)){
			out.println("<script>alert('정상적으로 등록 되었습니다. 로그인 하세요');document.location.href='../main.jsp';</script>");
			 //response.sendRedirect("../main.jsp");
		}
		else {
			out.println("<script>alert('같은 아이디가 존재합니다.');history.go(-1);</script>");
		}
	}
	
	//회원정보 수정페이지 요청
	else if(action.equals("modify")){
		ArrayList<Member> datas = ndao.getView(uid);
		request.setAttribute("mdview", datas);
		pageContext.forward("modify.jsp");
	}
	// 회원정보 수정 등록
	else if (action.equals("update")) {
		if(ndao.updateMember(member, uid)) {
			response.sendRedirect("mypage.jsp");
		}
		else
			throw new Exception("DB 갱신오류");
	}
	
	//로그인
	else if(action.equals("login")){
		if(ndao.login(member.getUid(),member.getPasswd())){
			//로그인 성공시 세션에 "uid"저장
			session.setAttribute("uid", member.getUid());
			
			if(target.equals("popup")) {
				out.println("<script>self.close();opener.location.reload();</script>");
			}else{
				response.sendRedirect(request.getHeader("referer"));
			}
		}
		else{
			out.println("<script>alert('아이디나 비밀번호가 틀렸습니다.');history.go(-1);</script>");
		}
	}
	//로그아웃
	else if(action.equals("logout")){
		//세션에 저장된 값 초기화
		session.removeAttribute("uid");
		
		response.sendRedirect("../main.jsp");
	}
	
	//회원탈퇴
	else if(action.equals("leave")){
		if(ndao.login(uid,member.getPasswd())){
			//로그인 성공시
			if(ndao.leaveMember(uid,member.getPasswd())){
				//세션에 저장된 값 초기화
				session.removeAttribute("uid");
				out.println("<script>alert('탈퇴가 완료 되었습니다.');document.location.href='../main.jsp';</script>");

			}else{ 
				out.println("<script>alert('다시 시도해 주세요.');history.go(-1);</script>");
			}
		}
		else{
			out.println("<script>alert('비밀번호가 틀렸습니다.');history.go(-1);</script>");
		}
	}
%>
