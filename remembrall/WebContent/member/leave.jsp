<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<jsp:include page="../include/header.jsp"/> 
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">Member</a> &gt;
				<span class="current">Mypage</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>Mypage</h1>
				</div>
				<div class="cont_section01">
					<div class="cont_section01">
						<ul class="tab01">
							<li><a href="./member_controller.jsp?action=modify">회원 정보 수정</a></li>
							<li class="on"><a href="leave.jsp">회원 탈퇴</a></li>
						</ul>
					</div>
					
					<p class="p_notice">탈퇴하시면 작성하신 노트가 모두 삭제됩니다!</p>	
					<form name="leaveForm" method="post" action="./member_controller.jsp?action=leave">
					<div class="log_section">
						<fieldset>
						<legend>로그인</legend>
							<input type="hidden" name="action" value="login" />
							<div class="left">
								<ul>
									<li>
										<label for="input_id">아이디</label>
										<input type="text" class="inputTxt" id="input_id" value="${uid}" style="cursor:text;background-color:#eee;" readonly="readonly" name="uid" />
									</li>
									<li>
										<label for="input_pwd">비밀번호</label>
										<input type="password" class="inputTxt" id="input_pwd" placeholder="비밀번호를 입력해 주세요." tabindex="1" name="passwd" />
									</li>
								</ul>
							</div>
							<div class="btn_login">
								<button type="submit" class="btn_lg_purple">회원 탈퇴</button>
							</div>
						</fieldset>
					</div>
					</form>
				</div>

			</div>
			<!-- //contents -->
		</div>
<jsp:include page="../include/footer.jsp"/> 