<%@ tag  body-content="scriptless" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:choose>
<c:when test="${uid != null}">
	${uid}
	<input type="hidden" name="action" value="logout">
	<li><input type="submit" value="로그아웃"></li>
</c:when>
<c:otherwise>
		<div class="log_section">
			<fieldset>
			<legend>로그인</legend>
				<input type="hidden" name="action" value="login" />
				<div class="left">
					<ul>
						<li>
							<label for="input_id">아이디</label>
							<input type="text" class="inputTxt" id="input_id" placeholder="아이디를 입력해 주세요." name="uid" />
						</li>
						<li>
							<label for="input_pwd">비밀번호</label>
							<input type="password" class="inputTxt" id="input_pwd" placeholder="비밀번호를 입력해 주세요." name="passwd" />
						</li>
					</ul>
					<!-- <span class="login_chk">
						<input type="checkbox" class="checkbox logincheck" id="logincheck" checked="checked" name="loginsave" />
						<label for="loginsave">자동 로그인</label>
					</span> -->
				</div>
				<div class="btn_login">
					<button type="submit" class="btn_lg_purple">로그인</button>
				</div>
			</fieldset>
		</div>
		<ul class="log_txt">
			<li>
				<span class="txt">아직 회원이 아니신가요? <br /> <em>리멤브럴</em>의 회원이 되시면 서비스를 이용 가능합니다.</span>
				<div class="right">
					<a href="<c:url value="/member/join.jsp"/>" class="btn_sm_gray">회원가입하기</a>
				</div>
			</li>
		</ul>
		<!-- <script>
			$('.btn_login button').on('click',function(){
				var frm = $('form');
				var isSavecheck = $('.logincheck').prop('checked');
				if(isSavecheck) $('input[name=loginsave]',frm).val(true);
				else $('input[name=loginsave]',frm).val(false);
				frm.submit();
				$('input[name=loginsave]',frm).val();
			});
		</script> -->
</c:otherwise>
</c:choose>