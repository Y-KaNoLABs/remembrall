create table member(
	uid varchar(15) NOT NULL,
    name varchar(15) NOT NULL,
    passwd varchar(12) NOT NULL,
    email varchar(45) DEFAULT NULL,
    date date NOT NULL,
	PRIMARY KEY(uid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table mynote(
	nid int(10) not null auto_increment,
    uid varchar(15) not null,
    notetit varchar(200) DEFAULT NULL,
    notebody varchar(5000) DEFAULT NULL,
    date datetime NOT NULL,
    mdate datetime default null,
	trashbox TINYINT(1) NOT NULL DEFAULT '0',
    primary key(nid, uid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table todo(
	tid int(10) not null auto_increment,
    uid varchar(15) not null,
    tit varchar(500) DEFAULT NULL,
    date varchar(100) DEFAULT NULL,
	done TINYINT(1) NOT NULL DEFAULT '0',
    primary key(tid, uid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

 