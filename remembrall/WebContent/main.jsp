<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8" import="mynote.*,member.*,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<%-- 메시지 처리 빈즈 --%>
<jsp:useBean id="note" class="mynote.Mynote" />
<jsp:useBean id="notedao" class="mynote.MynoteDAO" />
<%-- 프로퍼티 set  --%>
<jsp:setProperty name="note" property="*" />
<%
	// 기본 파라미터 정리

	// 다음 페이지 요청 카운트
	String cnt = request.getParameter("cnt");
	
	// 특정 회원 게시물 only
	String uid = (String)session.getAttribute("uid");
	
	// 홈 URL
	String home;
	
	// note 페이지 카운트
	int ncnt;
	int allCnt = notedao.getAllCnt(uid,false);
	
	if((cnt != null) && (uid !=null)) {
		// 각 action 처리후 메인으로 되돌아가기 위한 기본 url
		home = "main.jsp?action=getall&cnt="+cnt+"&uid="+uid;
		ncnt = Integer.parseInt(request.getParameter("cnt"));
	}
	else {
		// 게시글 작성시에는 현재 상태와 상관 없이 전체 게시물의 첫페이지로 이동 하기 위한 url
		home = "main.jsp?action=getall";
		// 첫페이지 요청인 경우, 기본 게시물 10개씩
		ncnt = 10;
	}
	ArrayList<Mynote> datas = notedao.getList(ncnt,uid);
	
	// 게시글 목록
	request.setAttribute("list", datas);		
	// 현재 페이지 카운트 정보 저장
	request.setAttribute("cnt",ncnt);
	// 전체 게시글 수 저장
	request.setAttribute("allCnt",allCnt);
	
	//pageContext.forward("main.jsp");
%>

<jsp:include page="include/header.jsp"/> 
		<div id="contents_wrap">
			<div class="location">
				<a href="<c:url value="/" />"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<span class="current">Main</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>Remembrall Note</h1>
				</div>
				<div class="cont_section01">
					<c:choose>
					<c:when test="${uid == null}">
					<form name="loginform" method="post" action="./member/member_controller.jsp">
					<input type="hidden" name="target" value="main" />
					<note:login />	
					</form>
					</c:when>
					<c:otherwise>
						<div class="main_cont">
							<div class="main_newnote">
								<h2 class="blind">My Note</h2>
								<form class="n_form" method="post" action="./mynote/mynote_controller.jsp?action=newnote">
									<input type="hidden" name="uid" value="${uid}">
									<input type="text" class="main_text" name="tit" maxlength="100" placeholder="제목" />
									<textarea class="main_text" name="body" maxlength="10000" placeholder="새 노트 쓰기" onclick="remembrall.action.mainForm();"></textarea>
									<button class="submit btn_md_purple" type="button" onclick="remembrall.action.boardWriteForm()">등록</button>
								</form>
							</div>
						</div><!-- //write -->
						
						
						<h2>My Note <span class="fr" style="margin-bottom:-10px;font-size:12px;font-family:'Noto Sans Thin';">총  ${allCnt} 개</span></h2>
						
						<c:if test="${not empty list}">
						<div class="list type2">
							<ul id="list">
								<c:forEach varStatus="ncnt" var="n" items="${list}">
								<li>
									<a href="mynote/mynote_controller.jsp?action=view&nid=${n.nid}">
										<!-- <span class="col1">[${n.nid}]</span> -->
										<span class="col2"><c:if test="${empty n.tit}">제목없음</c:if>
										<c:choose>
											<c:when test="${fn:length(n.tit) > 20}">
												<c:out value="${fn:substring(n.tit,0,19)}"/>...
											</c:when>
											<c:otherwise>
												<c:out value="${n.tit}"/>
											</c:otherwise> 
										</c:choose>
										
										</span>
										<span class="col3"><c:if test="${not empty n.mdate}">${n.mdate}</c:if><c:if test="${empty n.mdate}">${n.date}</c:if></span>
										<span class="col4"><c:if test="${empty n.body}">-</c:if>
										<c:set var = "string1" value = "${fn:replace(n.body, '<br />', ' ')}"/>
										<c:choose>
											<c:when test="${fn:length(n.body) > 50}">
												<c:out value="${fn:substring(string1,0,49)}"/>...
											</c:when>
											<c:otherwise>
												<c:out value="${string1}"/>
											</c:otherwise> 
										</c:choose>
										</span>
									</a>
								</li>
								</c:forEach>
							</ul>
						</div>
						<c:if test="${cnt < allCnt}"><!-- 전체 글 수 보다 같거나 크면 더보기 버튼 숨기기-->
						<div align="center"><a href="main.jsp?action=getall&cnt=${cnt+10}&uid=${n.uid}" class="btn_md_stroke full">더보기</a></div>
						</c:if>
						</c:if>
						<c:if test="${empty list}">
						<p class="list-defualt">등록된 노트가 없습니다.</p>
						</c:if>
						<!-- //list -->
					</c:otherwise>
					</c:choose>
				</div>

			</div>
			<!-- //contents -->
		</div>
<script>
$(window).on('load',function(){
	$('#list li').each(function(i){
		var name = 'style'+ (Math.floor(Math.random()*5)+1);
		$(this).addClass(name);
	});
});
</script>
<jsp:include page="include/footer.jsp"/> 