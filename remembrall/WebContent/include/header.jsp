<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=2.0, minimum-scale=1.0, user-scalable=no"/>
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="black" />
<link rel="shortcut icon" href="<c:url value="/rss/images/common/favicon.png" />" />
<link rel="shortcut icon" href="<c:url value="/rss/images/common/favicon.ico" />" />
<link rel="apple-touch-icon" href="<c:url value="/rss/images/common/favorite.png" />" />
<meta property="og:image" content="http://remembrall.cafe24.com/rss/images/common/share.jpg">
<meta property="og:image:width" content="300" /> 
<meta property="og:image:height" content="300" /> 
<meta property="og:type"  content="website" />
<meta property="og:url" content="http://remembrall.cafe24.com">
<meta property="og:site_name" content="Remembrall Note" />
<meta name="description" content="간편한 메모 작성" />
<meta name="keywords" content="리멤브럴, remembrall" />
<meta property="og:title" content="Remembrall Note" />
<meta id="facebook_title" property="og:title" content="리멤브럴노트" />
<meta id="facebook_url" property="og:url" content="http://remembrall.cafe24.com"/> 
<meta id="facebook_img" property="og:image" content="http://remembrall.cafe24.com/rss/images/common/share.jpg"/>
<meta id="facebook_desc" property="og:description" content="remembrall note"/>    
 <!-- 트위터 -->
<meta name="twitter:card"           content="summary_large_image">
<meta name="twitter:site"           content="리멤브럴 노트">
<meta name="twitter:image"          content="http://remembrall.cafe24.com/rss/images/common/share.jpg">
<!-- Google -->
<meta itemprop="name" content="리멤브럴노트">
<meta itemprop="description" content="Remembrall note ">
<meta itemprop="image" content="http://remembrall.cafe24.com/rss/images/common/share.jpg">
<meta name="mobile-web-app-capable" content="yes"><!-- 안드로이드 홈화면추가시 상단 주소창 제거 -->
<meta name="apple-mobile-web-app-capable" content="yes"><!-- ios홈화면추가시 상단 주소창 제거 -->
<title>Remembrall Note</title>
<link rel="stylesheet" href="<c:url value="/rss/css/font.css" />" type="text/css" media="screen" />
<link rel="stylesheet" href="<c:url value="/rss/css/global.css" />" type="text/css" media="screen" />
<link rel="stylesheet" href="<c:url value="/rss/css/layout.css" />" type="text/css" media="screen" />
<link rel="stylesheet" href="<c:url value="/rss/css/contents.css" />" type="text/css" media="screen" />
<script src="<c:url value="/rss/js/lib/jquery-2.2.3.min.js" />" ></script>
<script src="<c:url value="/rss/js/lib/jquery.easing.1.3.js" />" ></script>
<script src="<c:url value="/rss/js/common.js" />"></script>

<!--[if IE]><script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
</head>

<body>
<!-- skip navi -->
<div id="accessibility">
	<a href="#contents">본문내용 바로가기</a>
</div>
<!-- //skip navi -->
<div id="wrap">
	<header id="m_header">
		<h1><a href="<c:url value="/main.jsp"/>">리멤브럴 노트</a></h1>
		<a href="#" class="btn_gnb"><span class="blind">전체 메뉴</span></a>
	</header><!-- mobile용 header -->
	<!-- sidebar -->
	<div id="sidebar">
		<h1><a href="<c:url value="/main.jsp"/>">리멤브럴<br />노트</a></h1>

		<div class="globalmenu">
 			<h2 class="skip_tit">회원 메뉴</h2>
			<form name="loginform" method="post" action="${pageContext.request.contextPath}/member/member_controller.jsp">
			<c:choose>
			<c:when test="${uid == null}">
			<ul>
				<li><a href="<c:url value="/member/login.jsp"/>" onclick="window.open(this.href,'','width=669px, height=340px, scrollbars=no'); return false;" target="_blank">LOGIN</a></li>
				<li><a href="<c:url value="/member/join.jsp"/>">JOIN</a></li>
			</ul>
			</c:when>
			<c:otherwise>
			<li>${uid} 님</li>			
			<li><input type="hidden" name="action" value="logout"><input type="submit" value="LOGOUT"></li>
			<li><a href="<c:url value="/member/mypage.jsp"/>">MYPAGE</a></li>
			</c:otherwise>
			</c:choose>
			</form>
		</div>
		<!-- gnb -->
		<div class="gnbWrap">
			<h2 class="skip_tit">주메뉴</h2>
			<ul class="gnb">
				<li class="m01"><a href="#n">M Y&nbsp; &nbsp;N O T E</a>
					<div class="depth02">
						<ul>
							<li><a href="<c:url value="/mynote/mynote_controller.jsp?action=getall"/>">Note List</a></li>
							<li><a href="<c:url value="/mynote/write.jsp"/>">New Note</a></li>
						</ul>
					</div>
				</li>
				<li class="m02"><a href="<c:url value="/mynote/mynote_controller.jsp?action=gettrash"/>">TRASH BOX</a></li>
				<li class="m03"><a href="<c:url value="/todo/todo_controller.jsp?action=getall"/>">To-Do List</a></li>
			</ul>
			<div class="bg" style="display:;"></div><!-- for pc -->
		</div>
		<!-- //gnb -->
	</div>
	<!-- //sidebar -->
	
	<!-- mobile gnb -->
	<div id=m_gnb>
		<h1><a href="<c:url value="/main.jsp"/>">리멤브럴<br />노트</a></h1>

		<div class="globalmenu">
 			<h2 class="skip_tit">회원 메뉴</h2>
			<form name="loginform" method="post" action="${pageContext.request.contextPath}/member/member_controller.jsp">
			<c:choose>
			<c:when test="${uid == null}">
			<ul>
				<li><a href="<c:url value="/member/login.jsp"/>" onclick="window.open(this.href,'','width=669px, height=340px, scrollbars=no'); return false;" target="_blank">LOGIN</a></li>
				<li><a href="<c:url value="/member/join.jsp"/>">JOIN</a></li>
			</ul>
			</c:when>
			<c:otherwise>
			<li><span>${uid} 님</span></li>			
			<li><input type="hidden" name="action" value="logout"><button type="submit">LOGOUT</button></li>
			<li><a href="<c:url value="/member/mypage.jsp"/>">MYPAGE</a></li>
			</c:otherwise>
			</c:choose>
			</form>
		</div>
		<!-- gnb -->
		<div class="gnbWrap">
			<h2 class="skip_tit">주메뉴</h2>
			<ul class="gnb">
				<li class="m01"><a href="#n">M Y&nbsp; &nbsp;N O T E</a>
					<div class="depth02">
						<ul>
							<li><a href="<c:url value="/todo/mynote_controller.jsp?action=getall"/>">Note List</a></li>
							<li><a href="<c:url value="/mynote/write.jsp"/>">New Note</a></li>
						</ul>
					</div>
				</li>
				<li class="m02"><a href="<c:url value="/mynote/mynote_controller.jsp?action=gettrash"/>">TRASH BOX</a></li>
				<li class="m03"><a href="<c:url value="/todo/todo_controller.jsp?action=getall"/>">To-Do List</a></li>
			</ul>
		</div>
		<!-- //gnb -->
		<buttun class="btn_close"><span class="blind">gnb 닫기</span></buttun>
	</div>
	<!-- //mobile gnb -->
	<!-- container -->
	<div id="container">