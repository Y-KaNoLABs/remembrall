<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	</div>
	<!-- //container -->
	<!-- footer -->
	<footer id="footer">
		<h2 class="skip_tit">사이트정보</h2>
		<ul class="footer_navi">
			<li><a href="<c:url value="/info.jsp" />">이용 안내</a></li>
		</ul>

		<div class="footer_info">
			<p>copyright ⓒ 2018 Remembrall All Rights Reserved.</p>
		</div>
	</footer>
	<!-- footer -->
</div>
</body>
</html>