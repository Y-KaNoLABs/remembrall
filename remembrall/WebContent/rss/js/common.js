//'use strict';

var remembrall = {};
var resize_fn = [];//resize시 사용
var scroll_fn = [];//scroll시 사용
var wheel_fn = [];//wheel시 사용

var mFst=true;
var pcFst = true;
$(document).ready(function(){
	remembrall.util = util;
	remembrall.action = action;
	
	remembrall.action.resizeWin();

	resize_fn['header_resize']='remembrall.action.resizeWin();';
});
$(window).on('resize',function(){
	//resize
	for(var fn in resize_fn){
	  eval(resize_fn[fn]);
	}
}).on('scroll',function(){
	for(var fn in scroll_fn){
		eval(scroll_fn[fn]);
	}
}).on('orientationchange',function(){
	for(var fn in resize_fn){
	  eval(resize_fn[fn]);
	}
});


var util = {
	get_winW : function(){
		return $(window).width();
	},
	get_winH : function(){
		return $(window).height();
	},
	get_scrollT : function(){
		return $(window).scrollTop();
	},
	get_platform : function(){
		var platform_name = undefined;
		var filter = "win16|win32|win64|mac|macintel";
		if ( navigator.platform ){
			if ( filter.indexOf( navigator.platform.toLowerCase() ) < 0 ) { 
				platform_name = 'mogile';
			}else { 
				platform_name = 'pc'; 
			} 
		}
		return platform_name;
	},
	get_device : function(){
		var browser_name = undefined;
		var userAgent = navigator.userAgent.toLowerCase();
		switch(true){
			case /iphone/.test(userAgent) :
				browser_name = 'ios';
				break;
			case /naver/.test(userAgent) :
				browser_name = 'naver';
				break;
			case /msie 6/.test(userAgent) :
				browser_name = 'ie6';
				break;
			case /msie 7/.test(userAgent) :
				browser_name = 'ie7';
				break;
			case /msie 8/.test(userAgent) :
				browser_name = 'ie8';
				break;
			case /msie 9/.test(userAgent) :
				browser_name = 'ie9';
				break;
			case /msie 10/.test(userAgent) :
				browser_name = 'ie10';
				break;
			case /edge/.test(userAgent) :
				browser_name = 'edge';
				break;
			case /chrome/.test(userAgent) :
				browser_name = 'chrome';
				break;
			case /safari/.test(userAgent) :
				browser_name = 'safari';
				break;
			case /firefox/.test(userAgent) :
				browser_name = 'firefox';
				break;
			case /opera/.test(userAgent) :
				browser_name = 'opera';
				break;
			case /mac/.test(userAgent) :
				browser_name = 'mac';
				break;
			default :
				browser_name = 'unknown';
		}
		return browser_name;
	}
}

var action = {	
	header:function(){
		console.log('start header');
		var header = new Header();
			header.init();

		function Header(){
			var $sidebar = null,
				$depth1 = null,
				$depth2 = null,
				$gnb = null,
				$bg = null,
				$mGnb = null;
			
			Header.prototype.init = function(){
				$sidebar = $('#sidebar');
				$depth1 = $sidebar.find('.gnb>li');
				$depth2 = $sidebar.find('.depth02');
				$gnb = $sidebar.find('.gnbWrap');
				$bg = $sidebar.find('.bg');
				$mGnb = $('#m_gnb');
				$depth2.css({'display':'none','opacity':0});
				$bg.css({'display':'none', 'width':0, 'padding':0});					
				
				setResize();
				setEvent();
			}
			
			function setResize(){
				$(window).bind('resize', function(e){
					//onResize();
				});
			}
			function onResize(){
				//mobile버전
			}
			
			function setEvent(){
				$depth1.stop().bind('mouseenter focusin', function(e){
					$depth1.siblings().removeClass('on');
					$(this).addClass('on');
					if($(this).find($depth2).length>0) {
						openBG();
						$(this).stop().animate({'margin-bottom':70},800,'easeInOutCubic');
						$(this).find($depth2).stop().show().animate({'opacity':1},200);
					}
				});/*
				$depth2.stop().bind('mouseout focusout', function(e){
					$(this).parent($depth1).stop().animate({'margin-bottom':0},800,'easeInOutCubic');
					$(this).stop().animate({'opacity':0},200,function(){
						$(this).find($depth2).hide();
					});
				});*/
				$gnb.stop().bind('mouseleave focusout', function(e){
					$depth1.removeClass('on');
					$depth2.removeClass('on');
					$depth1.stop().animate({'margin-bottom':0},800,'easeInOutCubic');
					$depth2.stop().animate({'opacity':0},200,function(){
						$depth2.hide();
					});
					setTimeout(function(){
						closeBG();
					},200);
				});
				$depth2.find('li').stop().bind('mouseenter focusin', function(e){
					$(this).addClass('on');
				});
				$depth2.find('li').stop().bind('mouseout focusout', function(e){
					$(this).removeClass('on');
				});
			}
			
			function openBG(){
				$bg.css('display', 'block');
				$bg.stop().animate({width:124},200,'easeOutCubic');
			}
			function closeBG(){
				$bg.stop().animate({width:0},300,'easeInOutCubic',function(){
					$bg.css('display','none');
				});
			}
		}
	},//header
	mGnb:function(){
		console.log('start mobile gnb');
		var mgnb = new Mgnb();
		mgnb.init();

		function Mgnb(){
			var $mgnb = null,
				$btnClose = null,
				$btnGnb = null,
				$body = null;
			
			Mgnb.prototype.init = function(){
				$mgnb = $('#m_gnb'),
				$btnClose = $('.btn_close'),
				$btnGnb = $('.btn_gnb'),
				$body = $('body'),
				$sideBar = $('#sidebar');
				//$sideBar.hide();
				setEvent();
			}
			function setEvent(){
				$btnGnb.stop().bind('click',function(){
					$mgnb.show();
					$body.addClass('active_gnb');
					$('html').css({'oveflow':'hidden'});
					
					$mgnb.animate({height:'100%',opacity:1},600,'easeOutCubic');
				});
				$btnClose.stop().bind('click',function(){
					$body.removeClass('active_gnb');
					$('html').css({'oveflow':'auto'});
					$mgnb.animate({height:0,opacity:0},600,'easeOutCubic',function(){
						$mgnb.hide();
					});
				});
			}
			function setResize(){
				$(window).bind('resize', function(e){
					//onResize();
				});
			}
			function onResize(){
				//pc버전
			}
		}
	},//mobile gnb
	mainForm:function(){
		if(!$('.main_newnote').hasClass('active')){
			$('.main_newnote').addClass('active');
			$('.main_newnote').find('input[name=tit]').focus();
			/*$('.main_newnote').on('click',function(){
				$('.main_newnote').addClass('active');
			});*/
			$('html').click(function(e) {
				if(!$(e.target).hasClass("main_text")) { 
					$('.main_newnote').removeClass('active');
				} 
			});
		}
	},
	resizeWin:function(){
		if(remembrall.util.get_winW() >1024){
			$('#m_gnb').hide();
			$('#sidebar').show();
			if(pcFst==true){
				pcFst=false;
				remembrall.action.header();
			}
		}else{
			$('#sidebar').hide();
			if(mFst==true){
				mFst=false;
				remembrall.action.mGnb();
			}
		}
	},
	boardWriteForm:function(){
		var frm = $('.n_form');
		var input = $('input[name=tit]');
		var textarea = $('textarea[name=body]');
		var strBody = textarea.val();
		var strTit = input.val();
		strBody = (remembrall.action.convertHtml(strBody));
		strTit = (remembrall.action.convertHtml(strTit));
		textarea.val(strBody);
		input.val(strTit);
		frm.submit();
	},
	convertHtml:function(str){
		str = str.replace(/</g,"&lt;");
		str = str.replace(/>/g,"&gt;");
		str = str.replace(/\"/g,"&quot;");
		str = str.replace(/\'/g,"&#39;");
		str = str.replace(/\n/g,"<br />");
		return str;
	},
	boardViewForm:function(str){
		str = str.replace(/<br \/>/g, '\n');
		return str;
	}
//main form
}


