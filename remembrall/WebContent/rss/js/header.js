/**
 * @author ryuzuka
 */

//국문

var header = null;

$(document).ready(function(){
	header = new Header();
	header.init();
});

window.onload = function(){
	$(window).trigger(jQuery.Event("resize"));
}

function Header(){
	
	var $sidebar = null;
	var $footer = null;
	var $container = null;
	var $depth1 = null;
	var $depth2 = null;
	var $gnb = null;
	var $bg = null;
	
	var timer = null;
	var arrDepth1 = [];
	var arrDepth2 = [];
	var containerPaddingBottom = null;
	var isMain = false;
	
	
	Header.prototype.init = function(){
		
		$sidebar = $("#sidebar");
		$footer = $("#footer");
		$depth1 = $(".gnb>li");
		$depth2 = $(".depth02");
		$gnb = $(".gnbWrap");
		$bg = $(".bg");
		
		
		if($("#container").height() != null)
		{
			isMain = true;
			$container = $("#container");
		}
		else
		{										
			isMain = false;
			$container = $("#container");
		}
		containerPaddingBottom = parseInt($container.css("padding-bottom"));
		
		$depth1.find(".on").css({"background":"none"});
		//$depth1.find("img").eq(0).attr("src",$depth1.find("img").attr("src").replace("_on","_off"));
		$depth2.css({"display":"none","opacity":0});
		$bg.css({"display":"none", "width":0, "padding":0});
		$footer.css("height", 174);
		
		
		setResize();
		setMenu();
		setEvent();
	}
	
	function setResize(){
		$(window).bind("resize", function(e){
			//onResize();
		});
	}
	function onResize(){
		$sidebar.css("height", 0);
		$container.css("height",$(document).height());
		$footer.css("top", 0);
		if($(document).height() > 684){
			$footer.css("top", $(document).height() - ($footer.height() + 47));
			$sidebar.css("height", $(document).height());
		}else{
			$footer.css("top", 684-($footer.height() + 47));
			$sidebar.css("height", $(document).height() + 47);
		}
	}
	
	function setMenu(){
		$depth1.each(function(i){
			arrDepth1[i] = $(this);
		});
		$depth2.each(function(i){
			arrDepth2[i] = $(this);
		});
	}
	
	function setEvent(){
		for(var i=0; max=arrDepth1.length, i<max; i++)
		{
			arrDepth1[i].data("index", i);
			arrDepth1[i].bind("mouseenter focusin", function(e){
				var chkNum = $(this).data("index");
				clearInterval(timer);
				timer = null;
				if(chkNum != arrDepth1.length){
					menuOn(chkNum);
					if(chkNum == 0){
						TweenMax.to(this,0.8,{marginBottom:99,ease:Cubic.easeInOut});
					}else{
						TweenMax.to(this,0.8,{marginBottom:86,ease:Cubic.easeInOut});
					}
				}
			});
			arrDepth1[i].bind("mouseleave focusout", function(e){
				var chkNum = $(this).data("index");
				clearInterval(timer);
				timer = null;
				if(chkNum != arrDepth1.length){
					menuOn(chkNum);
					TweenMax.to(this,0.3,{marginBottom:0,ease:Cubic.easeInOut});
				}
			});
		}
		$gnb.bind("mouseleave focusout", function(e){
			timer = setTimeout(function(){
				closeBG();
				menuOn(1000);
			},200);
		});
		$('.gnb').children().each(setGNBDepth1);
		function setGNBDepth1(i){
			$(this).attr({depth1:i});
			$(this).find('li').each(setGNBDepth2);
		}
		function setGNBDepth2(i){
			$(this).attr({depth2:i}).mouseover(activateGNBDepth2).mouseout(activateGNBDepth2);
			$(this).attr({depth2:i}).focusin(activateGNBDepth2).focusout(activateGNBDepth2);
		}
		function activateGNBDepth2(e){
			var i = Number($(this).parent().parent().parent().attr('depth1'))+1;
			var ii = Number($(this).attr('depth2'))+1;
			switch(e.type){
				case 'mouseover':
					$(this).addClass('on');
				break;
				case 'mouseout':
					$(this).removeClass('on');
				break;
			}
			switch(e.type){
				case 'focusin':
					$(this).addClass('on');
				break;
				case 'focusout':
					$(this).removeClass('on');
				break;
			}
		}
	}
	
	function openBG(){
		$bg.css("display", "block")
		TweenMax.to($bg,0.2,{css:{"width":124},ease:Cubic.easeOut});
	}
	function closeBG(){
		TweenMax.to($bg,0.3,{css:{"width":0},ease:Cubic.easeInOut, onComplete:function(){
			$bg.css("display", "none");
		}});
	}
	
	function menuOn($chkNum){
		for(var i=0; i<arrDepth1.length; i++)
		{
			if(i == $chkNum)
			{
				//arrDepth1[i].css("background","none");
				//arrDepth1[i].find("img").eq(0).attr("src",arrDepth1[i].find("img").attr("src").replace("_off","_on"));
				if(i == arrDepth1.length)
					return;
				openBG();
				arrDepth1[i].addClass('on');
				arrDepth2[i].css({"display":"block"});
				TweenMax.to(arrDepth2[i],0.2,{css:{"opacity":1}, delay:0.2});
			}
			else
			{
				//arrDepth1[i].css("background","none");
				//arrDepth1[i].find("img").eq(0).attr("src",arrDepth1[i].find("img").attr("src").replace("_on","_off"));
				arrDepth1[i].removeClass('on');
				if(i == arrDepth1.length)
					return;
				arrDepth2[i].css({"display":"none"});
			}
		}
	}
}





