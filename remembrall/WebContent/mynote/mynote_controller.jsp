<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8" import="mynote.*,member.*,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.io.*"%>
<%@ page import="java.net.URLEncoder" %>

<!-- 메시지 처리 빈즈 -->
<jsp:useBean id="note" class="mynote.Mynote" />
<jsp:useBean id="notedao" class="mynote.MynoteDAO" />

<!-- 프로퍼티 set -->
<jsp:setProperty name="note" property="*" />

<%
	// 기본 파라미터 정리
	// 컨트롤러 요청 action 코드 값
	String action = request.getParameter("action");

	// 다음 페이지 요청 카운트
	String cnt = request.getParameter("cnt");
	
	// note id 요청
	String nid;

	// 특정 회원 게시물 only
	String uid = (String)session.getAttribute("uid");
	
	
	// 홈 URL
	String home;
	
	// note 페이지 카운트
	int ncnt;
	
	if((cnt != null) && (uid !=null)) {
		// 각 action 처리후 메인으로 되돌아가기 위한 기본 url
		home = "mynote_controller.jsp?action=getall&cnt="+cnt+"&uid="+uid;
		ncnt = Integer.parseInt(request.getParameter("cnt"));
	}
	else {
		// 게시글 작성시에는 현재 상태와 상관 없이 전체 게시물의 첫페이지로 이동 하기 위한 url
		home = "mynote_controller.jsp?action=getall";
		// 첫페이지 요청인 경우, 기본 게시물 10개씩
		ncnt = 10;
	}
	

	// 새로운 note 등록
	if (action.equals("newnote")) {
		if (notedao.newNote(note))
			response.sendRedirect(home);
		else
			throw new Exception("글쓰기 오류!!");
	}
	// note 삭제(휴지통으로 이동)
	else if (action.equals("delnote")) {
		nid = (String)request.getParameter("nid");
		if(notedao.delNote(nid,uid)) {
			response.sendRedirect(home);
		}
		else
			throw new Exception("삭제  오류!!");;
	} 
	// 휴지통에서 note 영구 삭제
	else if (action.equals("removenote")) {

		String delNote = request.getParameter("nidArr");
		System.out.println(delNote);
		String[] delArr = delNote.split(",");
		
		if(notedao.removeNote(delArr))
			out.println("<script>alert('삭제 되었습니다.');document.location.href='./mynote_controller.jsp?action=gettrash';</script>");			
		else
			throw new Exception("삭제  오류!!");;
	} 
	
	// 휴지통에서 복원
	else if (action.equals("reset")) {

		String reNote = request.getParameter("nidArr");
		System.out.println(reNote);
		String[] reArr = reNote.split(",");
		if(notedao.resetNote(reArr))
			out.println("<script>alert('선택한 노트가 복원 되었습니다.');document.location.href='./mynote_controller.jsp?action=gettrash';</script>");		
		else
			throw new Exception("복원  오류!!");;
	} 
	
	
	// 전체 게시글 가져오기
	else if (action.equals("getall")) {
		ArrayList<Mynote> datas = notedao.getList(ncnt,uid);
		int allCnt = notedao.getAllCnt(uid, false);
		// 게시글 목록
		request.setAttribute("list", datas);		
		// 현재 페이지 카운트 정보 저장
		request.setAttribute("cnt",ncnt);
		// 전체 게시글 수 저장
		request.setAttribute("allCnt",allCnt);
		pageContext.forward("list.jsp");
	}
	
	// 휴지통 목록 가져오기
		else if (action.equals("gettrash")) {
			ArrayList<Mynote> datas = notedao.getTrashList(ncnt,uid);
			int allCnt = notedao.getAllCnt(uid, true);
			
			// 게시글 목록
			request.setAttribute("list", datas);		
			// 현재 페이지 카운트 정보 저장
			request.setAttribute("cnt",ncnt);
			// 전체 게시글 수 저장
			request.setAttribute("allCnt",allCnt);
			pageContext.forward("trashbox.jsp");
		}
	
	//view
	else if (action.equals("view")) {
		nid = (String)request.getParameter("nid");
		ArrayList<Mynote> datas = notedao.getView(nid, uid);
		request.setAttribute("view", datas);
		pageContext.forward("view.jsp");
	}
	
	
	// 수정페이지 요청
	else if (action.equals("modify")) {
		nid = (String)request.getParameter("nid");
		ArrayList<Mynote> datas = notedao.getView(nid,uid);
		request.setAttribute("mdview", datas);
		pageContext.forward("modify.jsp");
	}
	// 수정 등록 요청인 경우
	else if (action.equals("update")) {
		nid = (String)request.getParameter("nid");
		if(notedao.modifyNote(note)) {
			response.sendRedirect("mynote_controller.jsp?action=view&nid="+nid);
		}
		else
			throw new Exception("DB 갱신오류");
	}
	
	//file 출력
	else if (action.equals("output")){
		nid = (String)request.getParameter("nid");
		ArrayList<Mynote> datas = notedao.getView(nid, uid);
		request.setAttribute("view", datas);
		
		String fileName = "fileoutput.txt"; //생성할 파일명
		String fileDir = "fileDir"; //파일을 생성할 디렉토리
		//String filePath = application.getRealPath(fileDir) + "/"; //파일을 생성할 전체경로
		String filePath = request.getSession().getServletContext().getRealPath(fileDir) + "/"; //파일을 생성할 전체경로
		File fPath = new File(filePath); //경로생성
		if ( !fPath.exists() ) {
			fPath.mkdirs(); //상위 디렉토리가 존재하지 않으면 상위디렉토리부터 생성.
		 }
		filePath += fileName; //생성할 파일명을 전체경로에 결합
	
		try{
			File f = new File(filePath); // 파일객체생성
			f.createNewFile(); //파일생성
			
			// 파일쓰기
			FileWriter fw = new FileWriter(filePath); //파일쓰기객체생성
			
			String data = "";
			for(int i=0; i < datas.size();i++){
				data=datas.get(i).getTit()+"\r\n \r\n"+
						datas.get(i).getDate()+"\r\n \r\n"+
						datas.get(i).getBody().replace("<br />", "\r\n");
				}
			fw.write(data); //파일에다 작성
			fw.close(); //파일핸들 닫기	
			
			String realPath = "../" + fileDir +"/"+ fileName;
		}catch (IOException e) { 
		  System.out.println(e.toString()); //에러 발생시 메세지 출력
		}
		finally{
			/*
				파일 다운로더 솔루션 출처 : http://zero-gravity.tistory.com/170
			*/
			
			String realPath = request.getSession().getServletContext().getRealPath(fileDir) + "/";
			System.out.println(filePath);
		  	// 파라미터로 받은 파일 이름.
		    String requestFileNameAndPath = fileName;
		     
		    // 서버에서 파일찾기 위해 필요한 파일이름(경로를 포함하고 있음)
		    // 한글 이름의 파일도 찾을 수 있도록 하기 위해서 문자셋 지정해서 한글로 바꾼다.
		    String UTF8FileNameAndPath = new String(requestFileNameAndPath.getBytes("8859_1"), "UTF-8");
		     
		    // 파일이름에서 path는 잘라내고 파일명만 추출한다.
 			String UTF8FileName = UTF8FileNameAndPath.substring(UTF8FileNameAndPath.lastIndexOf("/") + 1).substring(UTF8FileNameAndPath.lastIndexOf(File.separator) + 1);			
			// 브라우저가 IE인지 확인할 플래그.
		    boolean MSIE = request.getHeader("user-agent").indexOf("MSIE") != -1;
		     
		    // 파일 다운로드 시 받을 때 저장될 파일명
		    String fileNameToSave = "";
		 
		    // IE,FF 각각 다르게 파일이름을 적용해서 구분해주어야 한다.
		    if(MSIE){
		        // 브라우저가 IE일 경우 저장될 파일 이름
		        // 공백이 '+'로 인코딩된것을 다시 공백으로 바꿔준다.
		        fileNameToSave = URLEncoder.encode(UTF8FileName, "UTF8").replaceAll("\\+", " ");
		    }else{
		        // 브라우저가 IE가 아닐 경우 저장될 파일 이름
		        fileNameToSave = new String(UTF8FileName.getBytes("UTF-8"), "8859_1");
		    }
		 
		    // 다운로드 알림창이 뜨도록 하기 위해서 컨텐트 타입을 8비트 바이너리로 설정한다.
		    response.setContentType("application/octet-stream");
		     
		    // 저장될 파일 이름을 설정한다.
		    response.setHeader("Content-Disposition", "attachment; filename=\"" + fileNameToSave + "\";");
		     
		    // 파일패스 및 파일명을 지정한다.
		    //  String filePathAndName = pageContext.getServletContext().getRealPath("/") + UTF8FileNameAndPath;
		    String filePathAndName = realPath + UTF8FileNameAndPath;
		    File file = new File(filePathAndName);
		     
		    // 버퍼 크기 설정
		    byte bytestream[] = new byte[2048000];
		 
		    // response out에 파일 내용을 출력한다.
		    if (file.isFile() && file.length() > 0){
		         
		        FileInputStream fis = new FileInputStream(file);
		        BufferedInputStream bis = new BufferedInputStream(fis);
		        BufferedOutputStream bos = new BufferedOutputStream(response.getOutputStream());
		             
		        int read = 0;
		             
		        while ((read = bis.read(bytestream)) != -1){
		            bos.write(bytestream , 0, read);
		        }
		         
		        bos.close();
		        bis.close();
		         
		    }
		}
	}
%>