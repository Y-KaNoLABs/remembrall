<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/header.jsp"/>
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">MY NOTE</a> &gt;
				<span class="current">노트 목록</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>노트 목록</h1>
				</div>
				<div class="cont_section01">
					<div class="cboth">
						<a href="write.jsp" class="fr btn_sm_gray">새 글 쓰기</a>
					</div>
					<c:if test="${not empty list}">
					전체 글 수 : ${allCnt}
					<ul id="list" class="list type1">
						<c:forEach varStatus="ncnt" var="n" items="${list}">
						<li>
							<a href="mynote_controller.jsp?action=view&nid=${n.nid}">
								<!-- <span class="col1">[${n.nid}]</span> -->
								<span class="col2"><c:if test="${empty n.tit}">제목없음</c:if>${n.tit}</span>
								<span class="col3"><c:if test="${not empty n.mdate}">${n.mdate}</c:if><c:if test="${empty n.mdate}">${n.date}</c:if></span>
							</a>
						</li>
						</c:forEach>
					</ul>
						<c:if test="${cnt < allCnt}"><!-- 전체 글 수 보다 같거나 크면 더보기 버튼 숨기기-->
						<div align="center"><a href="mynote_controller.jsp?action=getall&cnt=${cnt+10}&uid=${n.uid}" class="btn_md_stroke full">더보기</a></div>
						</c:if>
					</c:if>
					<c:if test="${empty list}">
					<p class="list-defualt">등록된 노트가 없습니다.</p>
					</c:if>
				</div>

			</div>
			<!-- //contents -->
		</div>
<script>
$(documnet).ready(function(){
	
});
</script>
<jsp:include page="../include/footer.jsp"/> 

