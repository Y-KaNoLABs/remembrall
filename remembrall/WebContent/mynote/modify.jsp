<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/header.jsp"/>
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">MY NOTE</a> &gt;
				<span class="current">노트 수정</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>노트 수정</h1>
				</div>
				<div class="cont_section01">
					<c:forEach varStatus="0" var="n" items="${mdview}">
					<form class="n_form" method="post" action="mynote_controller.jsp?action=update&nid=${n.nid}">
					<input type=hidden name="nid" value="${n.nid}">
					<input type="hidden" name="uid" value="${n.uid}">
					<input type=hidden name="action" value="modify">
					<input type="text" name="tit" maxlength="20" value="<c:out value="${n.tit}"/>" />
					<textarea name="body" maxlength="500">${n.body}</textarea>
					<button class="submit btn_md_purple" type="button" onclick="remembrall.action.boardWriteForm()">수정</button>
					</c:forEach>
					</form>
				</div>

			</div>
			<!-- //contents -->
		</div>
<script>
$(window).on('load',function(){
	var strBody = $('textarea[name=body]').val();
	strBody = remembrall.action.boardViewForm(strBody);
	$('textarea[name=body]').val(strBody); 
});
</script>

<jsp:include page="../include/footer.jsp"/> 