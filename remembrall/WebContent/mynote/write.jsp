<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<jsp:include page="../include/header.jsp"/>
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">MY NOTE</a> &gt;
				<span class="current">새 노트 작성</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>새 노트 작성</h1>
				</div>
				<div class="cont_section01">
					<c:if test="${uid != null}">
					<form class="n_form" method="post" action="mynote_controller.jsp?action=newnote">
						<input type="hidden" name="uid" value="${uid}">
						<input type="text" name="tit" maxlength="100" placeholder="제목" />
						<textarea name="body" maxlength="10000" placeholder="새 노트 쓰기"></textarea>
						<button class="submit btn_md_purple" type="button" onclick="remembrall.action.boardWriteForm()">등록</button>
					</form>
					</c:if>
					<c:if test="${uid == null}">
						<p class="p_notice">작성하려면 로그인 하세요!</p>							
						<form name="loginform" method="post" action="${pageContext.request.contextPath}/member/member_controller.jsp">
							<input type="hidden" name="target" />
							<note:login />	
						</form>
					</c:if>
				</div>

			</div>
			<!-- //contents -->
		</div>
<jsp:include page="../include/footer.jsp"/> 