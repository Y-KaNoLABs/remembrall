<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="../include/header.jsp"/>
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">MY NOTE</a> &gt;
				<span class="current">휴지통</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>휴지통</h1>
				</div>
				<div class="cont_section01">
					<c:if test="${not empty list}">
					전체 글 수 : ${allCnt}
					<form id="trash_form" method="post" action="">
					<ul id="list" class="list type1">
						<c:forEach varStatus="ncnt" var="n" items="${list}">
						<li>
							<p>
								<label>
									<span class="col1"><input type="checkbox" name="RowCheck" value="${n.nid}" class="checkbox" /></span>
									<span class="col2"><c:if test="${empty n.tit}">제목없음</c:if>${n.tit}</span>
									<span class="col3"><c:if test="${not empty n.mdate}">${n.mdate}</c:if><c:if test="${empty n.mdate}">${n.date}</c:if></span>
								</label>
							</p>
						</li>
						</c:forEach>
					</ul>
					<c:if test="${empty list}">
						<div align="center"><a href="mynote_controller.jsp?action=trashbox&cnt=${cnt+10}&uid=${n.uid}" class="btn_md_stroke full">더보기</a></div>
					</c:if>
					<div class="control_box">
						<input type="checkbox" readonly="readonly" class="checkbox" disabled checked="checked" />선택 파일 
						<button type="button" class="btn_sm_gray" onclick="selectDel()">삭제</button>
						<button type="button" class="btn_sm_purple" onclick="selectReset()">복원</button>
					</div>
					</form>
					</c:if>
					<c:if test="${empty list}">
					<p class="list-defualt">데이터가 없습니다.</p>
					</c:if>
				</div>

			</div>
			<!-- //contents -->
		</div>
<script>
function selectDel(){
	var listForm = document.getElementById("trash_form");
	var nid = "";
	var nChk = document.getElementsByName('RowCheck');
	var checked=false;
	var indexid = false;
	for(i=0;i<nChk.length;i++){
		if(nChk[i].checked){
			if(indexid){
				nid = nid + ',';
			}
			nid=nid+nChk[i].value;
			indexid = true;
		}
	}
	if(!indexid){
		alert('삭제할 글을 체크해 주세요!');
		return;
	}
	listForm.action = "mynote_controller.jsp?action=removenote&nidArr="+nid;
	var agree=confirm('삭제 하시겠습니까?');
	if(agree){
		listForm.submit();
	}
}
function selectReset(){
	var listForm = document.getElementById("trash_form");
	var nid = "";
	var nChk = document.getElementsByName('RowCheck');
	var checked=false;
	var indexid = false;
	for(i=0;i<nChk.length;i++){
		if(nChk[i].checked){
			if(indexid){
				nid = nid + ',';
			}
			nid=nid+nChk[i].value;
			indexid = true;
		}
	}
	if(!indexid){
		alert('복원 할 글을 체크해 주세요!');
		return;
	}
	listForm.action = "mynote_controller.jsp?action=reset&nidArr="+nid;
	var agree=confirm('복원 하시겠습니까?');
	if(agree){
		listForm.submit();
	}
}
</script>
<jsp:include page="../include/footer.jsp"/> 

