<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="../include/header.jsp"/>
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">MY NOTE</a> &gt;
				<span class="current">노트 보기</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>노트 보기</h1>
				</div>
				<div class="cont_section01">
					<c:forEach varStatus="status" var="n" items="${view}">
					<div id="view" class="view_wrap">
						<div class="view_head">
							<p class="col2"><!-- <span class="col1">[ ${n.nid} ]</span> --> ${n.tit}</p>
							<p class="col3">작성일${n.date}, 수정일${n.mdate}</p>
						</div>
						<div class="view_body">
							${n.body}
						</div>
						<div class="control_box">
							<p class="fl"><a href="mynote_controller.jsp?action=output&nid=${n.nid}" target="_blank" class="btn_sm_purple">.TXT파일 받기</a></p>
							<p class="fr">
								<a href="mynote_controller.jsp?action=getall" class="btn_sm_gray">목록</a>
								<button type="button" onclick="delnote(${n.nid})" class="btn_sm_gray">삭제</button>
								<button type="button" onclick="document.location.href='mynote_controller.jsp?action=modify&nid=${n.nid}'" class="btn_sm_gray">수정</button>
							</p>
						</div>
					</div>
					</c:forEach>
				</div>

			</div>
			<!-- //contents -->
		</div>	
<script>
	function delnote(nid){
		var r= confirm("삭제 하시겠습니까?");
		if(r) document.location.href='mynote_controller.jsp?action=delnote&nid='+ nid;
		//console.log(nid);
	}
	/*$(window).on('load',function(){
		var strBody = $('.view_body').html();
		strBody = remembrall.action.boardViewForm(strBody);
		$('.view_body').html(strBody); 
	});*/
</script>
<jsp:include page="../include/footer.jsp"/> 