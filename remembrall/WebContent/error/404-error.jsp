<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>404 error</title>
</head>
<body>
<jsp:useBean id="now" class="java.util.Date" />
<body>
<div>
	<h2>404 Error</H2>
	<hr>
	<table>
	<tr bgcolor="pink">
		<td>
			요청하신 파일을 찾을 수 없습니다.<br />
			url 주소를 다시 한번 확인 해 주세요
		</td>
	</tr>
	<tr>
		<td>
			${now}<br />
			요청 실패 URI : ${pageContext.errorData.requestURI}<br />
			상태코드 : ${pageContext.errorData.statusCode}
		</td>
	</tr>
	</table>
</div>
</html>