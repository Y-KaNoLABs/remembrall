<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>error</title>
</head>
<body>
<jsp:useBean id="now" class="java.util.Date" />
<body>
<div>
	<h2>Error</H2>
	<hr>
	<table>
	<tr bgcolor="pink">
		<td>
			관리자에게 문의해 주세요..<br />
			빠른시일내 복구하겠습니다.
		</td>
	</tr>
	<tr>
		<td>
		${now}
		요청 실패 URI : ${pageContext.errorData.requestURI}<br />
		상태코드 : ${pageContext.errorData.statusCode}<br />
		예외유형 : ${pageContext.errorData.throwable}
		</td>
	</tr>
	</table>
</div>
</html>