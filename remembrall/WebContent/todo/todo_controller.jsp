<%@ page language="java" contentType="text/html;charset=UTF-8"
	pageEncoding="UTF-8" import="todo.*,mynote.*,member.*,java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="java.io.*"%>
<%@ page import="java.net.URLEncoder" %>

<!-- 메시지 처리 빈즈 -->
<jsp:useBean id="todo" class="todo.Todo" />
<jsp:useBean id="tododao" class="todo.TodoDAO" />

<!-- 프로퍼티 set -->
<jsp:setProperty name="todo" property="*" />

<%
	// 기본 파라미터 정리
	// 컨트롤러 요청 action 코드 값
	String action = request.getParameter("action");

	// todo id 요청
	String tid;

	// 특정 회원 게시물 only
	String uid = (String)session.getAttribute("uid");
	
	
	// 홈 URL
	String home;
	home = "todo_controller.jsp?action=getall";

	// 새로운 todo 등록
	if (action.equals("newtodo")) {
		if (tododao.newTodo(todo))
			response.sendRedirect(home);
		else
			throw new Exception("글쓰기 오류!!");
	}
	// todo 완료(done)
	else if (action.equals("donetodo")) {
		tid = (String)request.getParameter("tid");
		int isDone = 1;
		if(tododao.doneTodo(tid,uid,isDone)) {
			response.sendRedirect(home);
		}
		else
			throw new Exception("삭제  오류!!");;
	}

	// todo 완료 취소
	else if (action.equals("backtodo")) {
		tid = (String)request.getParameter("tid");
		int isDone = 0;
		if(tododao.doneTodo(tid,uid,isDone)) {
			response.sendRedirect(home);
		}
		else
			throw new Exception("삭제  오류!!");;
	} 
	// todo 영구 삭제
	else if (action.equals("removetodo")) {

		String delTodo = request.getParameter("tidArr");
		String[] delArr = delTodo.split(",");
		
		if(tododao.removeTodo(delArr))
			out.println("<script>alert('삭제 되었습니다.');document.location.href='./todo_controller.jsp?action=getall';</script>");			
		else
			throw new Exception("삭제  오류!!");;
	} 
	
	
	// 전체 게시글 가져오기
	else if (action.equals("getall")) {
		ArrayList<Todo> datas = tododao.getList(uid);
		// 게시글 목록
		request.setAttribute("list", datas);
		pageContext.forward("todo.jsp");
	}
%>