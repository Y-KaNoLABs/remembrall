<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="note"%>
<jsp:include page="../include/header.jsp"/>
		<div id="contents_wrap">
			<div class="location">
				<a href="#n"><img src="<c:url value="/rss/images/btn/btn_home.gif" />" alt="홈" /></a> &gt;
				<a href="#n">To Do List</a> &gt;
				<span class="current">list</span>
			</div>
			<!-- contents -->
			<div id="contents">
				<div class="cont_tit">
					<h1>To-Do List</h1>
				</div>
				<div class="cont_section01">
					<c:if test="${uid != null}">
					<div class="todo_write">	
						<form class="todo_form" method="post" action="./todo_controller.jsp?action=newtodo">
							<input type="hidden" name="uid" value="${uid}">
							<input type="text" class="date" name="date" maxlength="100" placeholder="YYYY-MM-DD HH:MM" />
							<input type="text" class="tit" name="tit" maxlength="500" placeholder="할 일을 입력 해 주세요" />
							<button class="submit btn_md_purple" type="submit">등록</button>
						</form>
					</div>
					</c:if>
					<c:if test="${uid == null}">
						<p class="p_notice">작성하려면 로그인 하세요!</p>							
						<form name="loginform" method="post" action="${pageContext.request.contextPath}/member/member_controller.jsp">
							<input type="hidden" name="target" />
							<note:login />	
						</form>
					</c:if>
					
					
					<%--
					<ul id="list" class="list type3">
						<li>
							<span class="action">
								<button type="button" title="완료"><span class="blind">완료</span></button>
							</span>
							<span class="date">2018.06.10 23:30</span>
							<span class="tit">알고리즘 과제</span>
							<span class="action2">
								<button type="button" title="삭제"><span class="blind">삭제</span></button>
							</span>
						</li>
						<li class="done">
							<span class="action">
								<button type="button" title="완료 됨"><span class="blind">완료 됨</span></button>
							</span>
							<span class="date">2018.06.04 23:30</span>
							<span class="tit">이메일 보내기!!!</span>
							<span class="action2">
								<button type="button" title="삭제"><span class="blind">삭제</span></button>
							</span>
						</li>
					</ul>
					 --%>
					<c:if test="${not empty list}">
					<ul id="list" class="list type3">
						<c:forEach varStatus="ncnt" var="t" items="${list}">
						<c:if test="${t.done==false}">
						<li>
							<span class="action">
								<button type="button" title="완료" onclick="document.location.href='./todo_controller.jsp?action=donetodo&tid=${t.tid}';"><span class="blind">완료</span></button>
							</span>
						</c:if>
						<c:if test="${t.done==true}">
						<li class="done">
							<span class="action">
								<button type="button" title="완료 됨" onclick="document.location.href='./todo_controller.jsp?action=backtodo&tid=${t.tid}';"><span class="blind">완료 됨</span></button>
							</span>
						</c:if>
							<span class="date">${t.date}</span>
							<span class="tit">${t.tit}</span>
							<span class="action2">
								<button type="button" title="삭제" onclick="document.location.href='./todo_controller.jsp?action=removetodo&tidArr=${t.tid}';"><span class="blind">삭제</span></button>
							</span>
						</li>
						</c:forEach>
					</ul>
					</c:if>
					<c:if test="${empty list}">
					<p class="list-defualt">등록된 할일이 없습니다.</p>
					</c:if>
					<%-- <c:if test="${not empty list}">
					전체 글 수 : ${allCnt}
					<ul id="list" class="list type1">
						<c:forEach varStatus="ncnt" var="n" items="${list}">
						<li>
							<a href="mynote_controller.jsp?action=view&nid=${n.nid}">
								<!-- <span class="col1">[${n.nid}]</span> -->
								<span class="col2"><c:if test="${empty n.tit}">제목없음</c:if>${n.tit}</span>
								<span class="col3"><c:if test="${not empty n.mdate}">${n.mdate}</c:if><c:if test="${empty n.mdate}">${n.date}</c:if></span>
							</a>
						</li>
						</c:forEach>
					</ul>
						<c:if test="${cnt < allCnt}"><!-- 전체 글 수 보다 같거나 크면 더보기 버튼 숨기기-->
						<div align="center"><a href="mynote_controller.jsp?action=getall&cnt=${cnt+10}&uid=${n.uid}" class="btn_md_stroke full">더보기</a></div>
						</c:if>
					</c:if>
					<c:if test="${empty list}">
					<p class="list-defualt">등록된 할일이 없습니다.</p>
					</c:if>--%>
				</div>

			</div>
			<!-- //contents -->
		</div>

<jsp:include page="../include/footer.jsp"/> 

